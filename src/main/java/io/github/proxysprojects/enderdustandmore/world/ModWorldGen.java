/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.world;

import io.github.proxysprojects.enderdustandmore.blocks.BlockEnderOre;
import io.github.proxysprojects.enderdustandmore.util.ModConfig;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.DimensionType;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenerator;
import net.minecraftforge.fml.common.IWorldGenerator;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Random;

public class ModWorldGen implements IWorldGenerator {

    private final WorldGenerator enderOreGen = new WorldGenEnderOre(BlockEnderOre.Variant.NORMAL, () -> Pair.of(ModConfig.enderOreMinSize, ModConfig.enderOreMaxSize), DimensionType.OVERWORLD);
    private final WorldGenerator netherEnderOreGen = new WorldGenEnderOre(BlockEnderOre.Variant.NETHER, () -> Pair.of(ModConfig.nEnderOreMinSize, ModConfig.nEnderOreMaxSize), DimensionType.NETHER);
    private final WorldGenerator endEnderOreGen = new WorldGenEnderOre(BlockEnderOre.Variant.END, () -> Pair.of(ModConfig.eEnderOreMinSize, ModConfig.eEnderOreMaxSize), DimensionType.THE_END);

    private Random random;
    private int chunkX;
    private int chunkZ;
    private World world;
    private IChunkGenerator chunkGenerator;
    private IChunkProvider chunkProvider;

    @Override
    public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {
        this.random = random;
        this.chunkX = chunkX;
        this.chunkZ = chunkZ;
        this.world = world;
        this.chunkGenerator = chunkGenerator;
        this.chunkProvider = chunkProvider;

        switch (world.provider.getDimensionType()) {
            case OVERWORLD:
                if (ModConfig.overworldOreGen)
                    generateOre(enderOreGen, ModConfig.enderOreFrequency, ModConfig.enderOreMinY, ModConfig.enderOreMaxY);
                break;
            case NETHER:
                if (ModConfig.netherOreGen)
                    generateOre(netherEnderOreGen, ModConfig.nEnderOreFrequency, ModConfig.nEnderOreMinY, ModConfig.nEnderOreMaxY);
                break;
            case THE_END:
                if (ModConfig.endOreGen)
                    generateOre(endEnderOreGen, ModConfig.eEnderOreFrequency, ModConfig.eEnderOreMinY, ModConfig.eEnderOreMaxY);
                break;
        }
    }

    // Taken from vanilla
    private void generateOre(WorldGenerator generator, int frequency, int minY, int maxY) {
        if (maxY < minY) {
            int i = minY;
            minY = maxY;
            maxY = i;
        }

        if (minY < 0)
            throw new IllegalArgumentException("Minimum Y out of bounds");
        if (maxY > 256)
            throw new IllegalArgumentException("Maximum Y out of bounds");

        int height = maxY - minY + 1;
        for (int i = 0; i < frequency; i++) {
            int x = chunkX * 16 + random.nextInt(16);
            int y = minY + random.nextInt(height);
            int z = chunkZ * 16 + random.nextInt(16);
            generator.generate(world, random, new BlockPos(x, y, z));
        }
    }
}