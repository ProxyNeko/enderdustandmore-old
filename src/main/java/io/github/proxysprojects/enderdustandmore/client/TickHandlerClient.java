/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.client;

import java.lang.reflect.Method;
import java.util.*;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import io.github.proxysprojects.enderdustandmore.util.CapeDownload;
import io.github.proxysprojects.enderdustandmore.util.IClientTicker;
import io.github.proxysprojects.enderdustandmore.util.ObfuscatedNames;
import io.github.proxysprojects.enderdustandmore.util.ReflectionUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraftforge.fml.client.FMLClientHandler;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.ClientTickEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.Phase;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import com.mojang.authlib.minecraft.MinecraftProfileTexture;

@SideOnly(Side.CLIENT)
public class TickHandlerClient {

    public static Minecraft minecraft = FMLClientHandler.instance().getClient();
    private static Set<IClientTicker> tickingSet = new HashSet<IClientTicker>();

    //TODO Create a cape for meeeee...(This one is a temp one, Same as donor..)
    private static final String PROXYNEKO_CAPE = "http://i.imgur.com/HbDUMmm.png";
    private Map<String, CapeDownload> proxynekoDownload = new HashMap<String, CapeDownload>();

    //TODO Create a donator cape for anyone who supports the mods development. (To be hosted on github)
    private static final String DONATE_CAPE = "http://i.imgur.com/HbDUMmm.png";
    private Map<String, CapeDownload> donateDownload = new HashMap<String, CapeDownload>();

    @SubscribeEvent
    public void onTick(ClientTickEvent event) {
        if(event.phase == Phase.START) {
            tickStart();
        }
    }

    public void tickStart() {
        if(!EnderDustAndMore.proxy.isPaused()) {
            for(Iterator<IClientTicker> iterator = tickingSet.iterator(); iterator.hasNext();) {
                IClientTicker ticker = iterator.next();
                if(ticker.needsTicks()) {
                    ticker.clientTick();
                }
                else {
                    iterator.remove();
                }
            }
        }

        if(minecraft.world != null && minecraft.player != null && !EnderDustAndMore.proxy.isPaused()) {
            for(EntityPlayer entityPlayer : minecraft.world.playerEntities) {
                if(entityPlayer instanceof AbstractClientPlayer) {
                    AbstractClientPlayer player = (AbstractClientPlayer)entityPlayer;

                    if(StringUtils.stripControlCodes(player.getName()).equals("ProxyNeko")) {
                        CapeDownload download = proxynekoDownload.get(player.getName());
                        if(download == null) {
                            download = new CapeDownload(player.getName(), PROXYNEKO_CAPE);
                            proxynekoDownload.put(player.getName(), download);
                            download.start();
                        }
                        else {
                            if(!download.downloaded) {
                                continue;
                            }
                            setCape(player, download.getResourceLocation());
                        }
                    }

                    if(EnderDustAndMore.donators.contains(StringUtils.stripControlCodes(player.getName()))) {
                        CapeDownload download = donateDownload.get(player.getName());
                        if(download == null) {
                            download = new CapeDownload(player.getName(), DONATE_CAPE);
                            donateDownload.put(player.getName(), download);
                            download.start();
                        }
                        else {
                            if(!download.downloaded) {
                                continue;
                            }
                            setCape(player, download.getResourceLocation());
                        }
                    }
                }
            }
        }
    }

    public static void setCape(AbstractClientPlayer player, ResourceLocation cape) {
        try {
            Method m = ReflectionUtils.getPrivateMethod(AbstractClientPlayer.class, ObfuscatedNames.AbstractClientPlayer_getPlayerInfo);
            Object obj = m.invoke(player);

            if(obj instanceof NetworkPlayerInfo) {
                NetworkPlayerInfo info = (NetworkPlayerInfo)obj;
                Map<MinecraftProfileTexture.Type, ResourceLocation> map = (Map<MinecraftProfileTexture.Type, ResourceLocation>)ReflectionUtils.getPrivateValue(info, NetworkPlayerInfo.class, ObfuscatedNames.NetworkPlayerInfo_playerTextures);
                map.put(MinecraftProfileTexture.Type.CAPE, cape);
            }
        } catch(Exception exception) {}
    }
}