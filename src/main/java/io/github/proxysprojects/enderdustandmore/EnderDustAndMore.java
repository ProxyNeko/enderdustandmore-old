/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore;

import io.github.proxysprojects.enderdustandmore.blocks.ModBlocks;
import io.github.proxysprojects.enderdustandmore.proxy.CommonProxy;
import io.github.proxysprojects.enderdustandmore.util.GetDataThread;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLMissingMappingsEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

@Mod
        (
                name = EnderDustAndMore.NAME,
                modid = EnderDustAndMore.MODID,
                version = EnderDustAndMore.VERSION,
                acceptedMinecraftVersions = "GRADLE:MC_VERSION",
                guiFactory = "io.github.proxysprojects.enderdustandmore.client.ModGuiFactory"
        )
public class EnderDustAndMore {
    public static final String NAME = "GRADLE:NAME";
    public static final String MODID = "GRADLE:MODID";
    public static final String VERSION = "GRADLE:VERSION";
    public static final CreativeTabs CREATIVE_TAB = new CreativeTabs(MODID) {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(ModBlocks.BLOCK_ENDER_ORE);
        }
    };

    @SidedProxy(clientSide = "io.github.proxysprojects.enderdustandmore.proxy.ClientProxy", serverSide = "io.github.proxysprojects.enderdustandmore.proxy.CommonProxy")
    public static CommonProxy proxy;

    public static Logger logger;

    public static List<String> donators = new ArrayList<String>();

    static {
        FluidRegistry.enableUniversalBucket();
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        logger = event.getModLog();
        logger.info("ProxyNeko is creating a new ore...");
        proxy.preInit(event);
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        logger.info("Sending in the Endermenz...");
        new GetDataThread();
        proxy.init(event);
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        logger.info("Endermenz ready to scare you!");
        proxy.postInit(event);
    }

    @Mod.EventHandler
    public void missingMappings(FMLMissingMappingsEvent event) {
        for (FMLMissingMappingsEvent.MissingMapping mapping : event.get()) {
            String name = mapping.resourceLocation.getResourcePath();
            if (name.equals("nether_ender_ore") || name.equals("end_ender_ore")) {
                switch (mapping.type) {
                    case BLOCK:
                        mapping.remap(ModBlocks.BLOCK_ENDER_ORE);
                        break;
                    case ITEM:
                        mapping.remap(Item.getItemFromBlock(ModBlocks.BLOCK_ENDER_ORE));
                        break;
                }
            }
        }
    }
}