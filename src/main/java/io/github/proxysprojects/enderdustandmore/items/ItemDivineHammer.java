/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.items;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import io.github.proxysprojects.enderdustandmore.util.ModConfig;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.entity.effect.EntityLightningBolt;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

public class ItemDivineHammer extends Item {

    public ItemDivineHammer() {
        setUnlocalizedName(EnderDustAndMore.MODID + ".divine_hammer");
        setRegistryName("divine_hammer");
        setMaxStackSize(1);
        setMaxDamage(0);
        setCreativeTab(EnderDustAndMore.CREATIVE_TAB);
    }

    @SideOnly(Side.CLIENT)
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
        ItemStack itemstack = player.getHeldItem(hand);
        if (hand == EnumHand.MAIN_HAND) {
            Random rand = new Random();
            int configChance = ModConfig.smiteFail;
            int chance;
            int range = ModConfig.smiteRange;
            double x, y, z;

            if (configChance == 0)
                chance = 1;
            else
                chance = rand.nextInt(configChance);

            if (range < 1 || range > 320)
                range = 128;

            if (chance == 0) {
                player.sendMessage(new TextComponentTranslation("edam.smite.msg"));
                x = player.posX;
                y = player.posY;
                z = player.posZ;
            } else {
                x = player.rayTrace(range, 1).getBlockPos().getX();
                y = player.rayTrace(range, 1).getBlockPos().getY();
                z = player.rayTrace(range, 1).getBlockPos().getZ();
            }

            if (!player.canPlayerEdit(new BlockPos(x, y, z), player.getHorizontalFacing(), itemstack))
                player.sendMessage(new TextComponentTranslation("edam.smite.protect"));
            else world.spawnEntity(new EntityLightningBolt(world, x, y, z, false));
        }
        return new ActionResult<>(EnumActionResult.PASS, itemstack);
    }

    public void registerItem() {
        GameRegistry.register(this);
    }

    @SideOnly(Side.CLIENT)
    public void initializeModels() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }
}