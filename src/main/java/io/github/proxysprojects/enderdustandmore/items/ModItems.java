/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.items;

import io.github.proxysprojects.enderdustandmore.util.ModConfig;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class ModItems {

    public static final ModItem ENDER_DUST = new ModItem("ender_dust");
    public static final ModItem ENDER_NUGGET = new ModItem("ender_nugget");
    public static final ModItem ENDER_INGOT = new ModItem("ender_ingot");
    public static final ModItem LIQUID_ENDER_BOTTLE = new ModItem("liquid_ender_bottle");
    public static final ModItem DUST_ENDER_BOTTLE = new ModItem("dust_ender_bottle");
    public static final ModItem DIVINE_ROD = new ModItem("divine_rod");
    public static final ModItem DIVINE_HAMMER_HEAD = new ModItem("divine_hammer_head");
    public static final ItemDivineHammer DIVINE_HAMMER = new ItemDivineHammer();

    public static void init() {
        registerItems();
        registerOres();
    }

    private static void registerItems() {
        ENDER_DUST.registerItem();
        ENDER_NUGGET.registerItem();
        ENDER_INGOT.registerItem();
        DUST_ENDER_BOTTLE.registerItem();
        LIQUID_ENDER_BOTTLE.registerItem();
        if (ModConfig.divineHammer) {
            DIVINE_ROD.registerItem();
            DIVINE_HAMMER_HEAD.registerItem();
            DIVINE_HAMMER.registerItem();
        }
    }

    private static void registerOres() {
        //Ender Dust names
        OreDictionary.registerOre("dustEnder", ModItems.ENDER_DUST);
        OreDictionary.registerOre("dustEnderpearl", ModItems.ENDER_DUST);
        OreDictionary.registerOre("nuggetEnder", ModItems.ENDER_NUGGET);
        OreDictionary.registerOre("ingotEnder", ModItems.ENDER_INGOT);
        //Liquid Ender names
        OreDictionary.registerOre("bottleLiquidEnderpearl", ModItems.LIQUID_ENDER_BOTTLE);
        OreDictionary.registerOre("bottleLiquidEnder", ModItems.LIQUID_ENDER_BOTTLE);
        //Bottled Ender Dust names
        OreDictionary.registerOre("bottleDustEnderpearl", ModItems.DUST_ENDER_BOTTLE);
        OreDictionary.registerOre("bottleDustEnder", ModItems.DUST_ENDER_BOTTLE);
        if (ModConfig.divineHammer) {
            OreDictionary.registerOre("divineRod", ModItems.DIVINE_ROD);
            OreDictionary.registerOre("divineHammerHead", ModItems.DIVINE_HAMMER_HEAD);
            OreDictionary.registerOre("divineHammer", ModItems.DIVINE_HAMMER);
        }
    }

    @SideOnly(Side.CLIENT)
    public static void initializeModels() {
        ENDER_DUST.initializeModels();
        ENDER_NUGGET.initializeModels();
        ENDER_INGOT.initializeModels();
        LIQUID_ENDER_BOTTLE.initializeModels();
        DUST_ENDER_BOTTLE.initializeModels();
        if (ModConfig.divineHammer) {
            DIVINE_ROD.initializeModels();
            DIVINE_HAMMER_HEAD.initializeModels();
            DIVINE_HAMMER.initializeModels();
        }
    }
}