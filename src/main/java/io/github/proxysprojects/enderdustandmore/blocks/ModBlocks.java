/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.blocks;

import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

public class ModBlocks {

    public static final BlockEnderOre BLOCK_ENDER_ORE = new BlockEnderOre();
    public static final BlockMysteriousTorch MYSTERIOUS_TORCH = new BlockMysteriousTorch();

    public static void init() {
        registerBlocks();
        registerOres();
    }

    private static void registerBlocks() {
        BLOCK_ENDER_ORE.registerBlock();
        MYSTERIOUS_TORCH.registerBlock();
    }

    private static void registerOres() {
        OreDictionary.registerOre("oreEnder", new ItemStack(ModBlocks.BLOCK_ENDER_ORE, 1, OreDictionary.WILDCARD_VALUE));
        OreDictionary.registerOre("blockMysteriousTorch", ModBlocks.MYSTERIOUS_TORCH);
    }

    @SideOnly(Side.CLIENT)
    public static void initializeModels() {
        BLOCK_ENDER_ORE.initializeModels();
        MYSTERIOUS_TORCH.initializeModels();
    }
}