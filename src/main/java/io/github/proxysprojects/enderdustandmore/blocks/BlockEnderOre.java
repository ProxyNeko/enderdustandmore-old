/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.blocks;

import com.google.common.base.Supplier;
import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import io.github.proxysprojects.enderdustandmore.items.ModItems;
import io.github.proxysprojects.enderdustandmore.util.ModConfig;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.properties.PropertyEnum;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.client.renderer.block.statemap.StateMap;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityList;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemMultiTexture;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.util.FakePlayer;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.util.Random;

import javax.annotation.Nonnull;

public class BlockEnderOre extends Block {

    public static final PropertyEnum<Variant> VARIANT = PropertyEnum.create("variant", Variant.class);
    public static final PropertyBool LIT = PropertyBool.create("lit");

    public BlockEnderOre() {
        super(Material.ROCK);
        setRegistryName("ender_ore");
        setUnlocalizedName(EnderDustAndMore.MODID + ".ender_ore");
        setCreativeTab(EnderDustAndMore.CREATIVE_TAB);
        setDefaultState(getBlockState().getBaseState().withProperty(VARIANT, Variant.NORMAL).withProperty(LIT, false));
        setTickRandomly(true);
        setHardness(4.0F);
        setResistance(60);
        setHarvestLevel("pickaxe", 3);
    }

    @Override
    public MapColor getMapColor(IBlockState state) {
        return state.getValue(VARIANT).getMapColor();
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void getSubBlocks(@Nonnull Item item, CreativeTabs tab, NonNullList<ItemStack> list) {
        for (Variant variant : Variant.values())
            list.add(new ItemStack(item, 1, variant.getMeta()));
    }

    @Override
    public void dropBlockAsItemWithChance(World world, BlockPos pos, IBlockState state, float chance, int fortune) {
        super.dropBlockAsItemWithChance(world, pos, state, chance, fortune);

        EntityPlayer player = harvesters.get();
        if (player instanceof FakePlayer && !ModConfig.fakePlayersSpawnEndermen) return;

        if (!world.isRemote && world.rand.nextDouble() <= state.getValue(VARIANT).getEndermanSpawnChance()) {
            EntityEnderman enderman = (EntityEnderman) EntityList.newEntity(EntityEnderman.class, world);
            if (enderman != null) {
                double x = pos.getX() + 0.5;
                double y = pos.getY() + 1;
                double z = pos.getZ() + 0.5;
                float yaw = world.rand.nextFloat() * 360F;
                float pitch = 0.0F;
                enderman.setLocationAndAngles(x, y, z, yaw, pitch);
                world.spawnEntity(enderman);
                enderman.playLivingSound();
            }
        }
    }

    @Override
    public int quantityDroppedWithBonus(int fortune, @Nonnull Random rand) {
        return quantityDropped(rand) + rand.nextInt(fortune + 1);
    }

    @Override
    public int quantityDropped(Random rand) {
        return 1 + rand.nextInt(3);
    }

    @Nonnull
    @Override
    public Item getItemDropped(IBlockState state, Random rand, int fortune) {
        return ModItems.ENDER_DUST;
    }

    @Override
    public int getExpDrop(IBlockState state, IBlockAccess world, BlockPos pos, int fortune) {
        Random random = world instanceof World ? ((World) world).rand : RANDOM;
        return 1 + random.nextInt(4);
    }

    @Override
    protected ItemStack getSilkTouchDrop(IBlockState state) {
        return new ItemStack(Item.getItemFromBlock(this), 1, state.getValue(VARIANT).getMeta());
    }

    @Nonnull
    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, VARIANT, LIT);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return getDefaultState().withProperty(VARIANT, Variant.getFromMeta(meta & 0b011)).withProperty(LIT, (meta & 0b100) != 0);
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        int meta = 0b000;
        meta |= state.getValue(VARIANT).getMeta(); // 0b011
        if (state.getValue(LIT))
            meta |= 0b100;
        return meta;
    }

    @Override
    public int getLightValue(@Nonnull IBlockState state, IBlockAccess world, @Nonnull BlockPos pos) {
        return state.getValue(LIT) ? 9 : 0;
    }

    @Override
    public int tickRate(World world) {
        return 30;
    }

    @Override
    public void onBlockClicked(World world, BlockPos pos, EntityPlayer player) {
        activate(world, pos, world.getBlockState(pos));
        super.onBlockClicked(world, pos, player);
    }

    @Override
    public void onEntityWalk(World world, BlockPos pos, Entity entity) {
        activate(world, pos, world.getBlockState(pos));
        super.onEntityWalk(world, pos, entity);
    }

    @Override
    public boolean onBlockActivated(World world, BlockPos pos, IBlockState state, EntityPlayer player, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        activate(world, pos, state);
        return super.onBlockActivated(world, pos, state, player, hand, facing, hitX, hitY, hitZ);
    }

    @Override
    public void updateTick(World world, BlockPos pos, IBlockState state, Random rand) {
        if (state.getValue(LIT))
            world.setBlockState(pos, state.withProperty(LIT, false));
    }

    @Override
    public void randomDisplayTick(IBlockState state, World world, BlockPos pos, Random rand) {
        if (state.getValue(LIT))
            spawnParticles(world, pos);
    }

    private void activate(World world, BlockPos pos, IBlockState state) {
        spawnParticles(world, pos);
        if (!state.getValue(LIT))
            world.setBlockState(pos, state.withProperty(LIT, true));
    }

    private void spawnParticles(World world, BlockPos pos) {
        AxisAlignedBB aabb = new AxisAlignedBB(pos.getX(), pos.getY(), pos.getZ(), pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1);
        double particleDistance = 0.0625;
        for (EnumFacing side : EnumFacing.VALUES) {
            double x = aabb.minX + world.rand.nextFloat();
            double y = aabb.minY + world.rand.nextFloat();
            double z = aabb.minZ + world.rand.nextFloat();
            if (!world.getBlockState(pos.add(side.getDirectionVec())).isOpaqueCube()) {
                switch (side) {
                    case UP:
                        y = aabb.maxY + particleDistance;
                        break;
                    case DOWN:
                        y = aabb.minY - particleDistance;
                        break;
                    case SOUTH:
                        z = aabb.maxZ + particleDistance;
                        break;
                    case NORTH:
                        z = aabb.minZ - particleDistance;
                        break;
                    case EAST:
                        x = aabb.maxX + particleDistance;
                        break;
                    case WEST:
                        x = aabb.minX - particleDistance;
                        break;
                }
            }
            if (x < aabb.minX || x > aabb.maxX || y < aabb.minY || y > aabb.maxY || z < aabb.minZ || z > aabb.maxZ)
                world.spawnParticle(EnumParticleTypes.REDSTONE, x, y, z, 0.9, 0.3, 1.0); // R=0.9,G=0.3,B=1
        }
    }

    public void registerBlock() {
        GameRegistry.register(this);
        GameRegistry.register(new ItemMultiTexture(this, this, (s) -> Variant.getFromMeta(s.getMetadata()).getName()), getRegistryName());
    }

    @SideOnly(Side.CLIENT)
    public void initializeModels() {
        Item item = Item.getItemFromBlock(this);
        ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(EnderDustAndMore.MODID + ":ender_ore", "inventory"));
        ModelLoader.setCustomModelResourceLocation(item, 1, new ModelResourceLocation(EnderDustAndMore.MODID + ":nether_ender_ore", "inventory"));
        ModelLoader.setCustomModelResourceLocation(item, 2, new ModelResourceLocation(EnderDustAndMore.MODID + ":end_ender_ore", "inventory"));
        ModelLoader.setCustomStateMapper(this, new StateMap.Builder().ignore(LIT).build());
    }

    public enum Variant implements IStringSerializable {
        NORMAL(0, "normal", MapColor.STONE, () -> ModConfig.endermanSpawn),
        NETHER(1, "nether", MapColor.NETHERRACK, () -> ModConfig.nEndermanSpawn),
        END(2, "end", MapColor.SAND, () -> ModConfig.eEndermanSpawn);

        private static final Variant[] VARIANTS = Variant.values();
        private final int meta;
        private final String name;
        private final MapColor mapColor;
        private final Supplier<Double> endermanSpawn;

        Variant(int meta, String name, MapColor mapColor, Supplier<Double> endermanSpawn) {
            this.meta = meta;
            this.name = name;
            this.mapColor = mapColor;
            this.endermanSpawn = endermanSpawn;
        }

        @Nonnull
        @Override
        public String getName() {
            return name;
        }

        public int getMeta() {
            return meta;
        }

        public MapColor getMapColor() {
            return mapColor;
        }

        public double getEndermanSpawnChance() {
            return endermanSpawn.get();
        }

        public static Variant getFromMeta(int meta) {
            return meta >= 0 && meta < VARIANTS.length ? VARIANTS[meta] : VARIANTS[0];
        }
    }
}
