/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.util;

import java.io.File;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

@SideOnly(Side.CLIENT)
public class CapeDownload extends Thread {

    public String username;
    public String staticCapeUrl;
    public ResourceLocation resourceLocation;
    public ThreadDownloadImageData capeImage;
    public boolean downloaded = false;

    public CapeDownload(String name, String url) {
        username = name;
        staticCapeUrl = url;
        setDaemon(true);
        setName("Cape Download Thread");
    }

    @Override
    public void run() {
        try {
            download();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private void download() {
        try {
            resourceLocation = new ResourceLocation("edam/" + StringUtils.stripControlCodes(username));
            capeImage = downloadCape();
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        downloaded = true;
    }

    public ThreadDownloadImageData getImage() {
        return capeImage;
    }

    public ResourceLocation getResourceLocation() {
        return resourceLocation;
    }

    public ThreadDownloadImageData downloadCape() {
        try {
            File capeFile = new File(resourceLocation.getResourcePath() + ".png");
            if(capeFile.exists()) {
                capeFile.delete();
            }
            TextureManager manager = Minecraft.getMinecraft().getTextureManager();
            ThreadDownloadImageData data = new ThreadDownloadImageData(capeFile, staticCapeUrl, null, null);
            manager.loadTexture(resourceLocation, data);
            return data;
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}