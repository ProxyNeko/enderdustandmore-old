/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public final class ReflectionUtils {
    /**
     * Retrieves a private value from a defined class and field.
     * @param obj - the Object to retrieve the value from, null if static
     * @param c - Class to retrieve field value from
     * @param fields - possible names of field to iterate through
     * @return value as an Object, cast as necessary
     */
    public static Object getPrivateValue(Object obj, Class c, String[] fields) {
        for(String field : fields) {
            try {
                Field f = c.getDeclaredField(field);
                f.setAccessible(true);
                return f.get(obj);
            }
            catch(Exception e) {
                continue;
            }
        }
        return null;
    }

    /**
     * Retrieves a private method from a class, sets it as accessible, and returns it.
     * @param c - Class the method is located in
     * @param methods - possible names of the method to iterate through
     * @param params - the Types inserted as parameters into the method
     * @return private method
     */
    public static Method getPrivateMethod(Class c, String[] methods, Class... params) {
        for(String method : methods) {
            try {
                Method m = c.getDeclaredMethod(method, params);
                m.setAccessible(true);
                return m;
            }
            catch(Exception e) {
                continue;
            }
        }
        return null;
    }
}