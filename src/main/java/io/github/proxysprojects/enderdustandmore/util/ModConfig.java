/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.util;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import net.minecraft.util.math.MathHelper;
import net.minecraftforge.common.config.ConfigCategory;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Mod.EventBusSubscriber(modid = EnderDustAndMore.MODID)
public class ModConfig {

    public static final String CATEGORY_OVERWORLD = "overworld";
    public static final String CATEGORY_NETHER = "nether";
    public static final String CATEGORY_END = "end";
    public static final String CATEGORY_MISC = "misc";

    private static final String LANG_PREFIX_PROPERTY = EnderDustAndMore.MODID + ".configgui.";
    private static final String LANG_PREFIX_CATEGORY = EnderDustAndMore.MODID + ".configgui.ctgy.";
    
    private static final String LANG_KEY_ENDER_ORE_GEN = "enderOreGen";
    private static final String LANG_KEY_ENDER_ORE_FREQUENCY = "enderOreFrequency";
    private static final String LANG_KEY_ENDER_ORE_MIN_Y = "enderOreMinY";
    private static final String LANG_KEY_ENDER_ORE_MAX_Y = "enderOreMaxY";
    private static final String LANG_KEY_ENDER_ORE_MAX_SIZE = "enderOreMaxSize";
    private static final String LANG_KEY_ENDER_ORE_MIN_SIZE = "enderOreMinSize";
    private static final String LANG_KEY_ENDERMAN_SPAWN = "endermanSpawn";

    private static Configuration config;

    //Overworld config
    public static int enderOreMinY;
    public static int enderOreMaxY;
    public static int enderOreFrequency;
    public static int enderOreMaxSize;
    public static int enderOreMinSize;
    public static double endermanSpawn;
    public static boolean overworldOreGen;

    //Nether config
    public static int nEnderOreMinY;
    public static int nEnderOreMaxY;
    public static int nEnderOreFrequency;
    public static int nEnderOreMaxSize;
    public static int nEnderOreMinSize;
    public static double nEndermanSpawn;
    public static boolean netherOreGen;

    //End config
    public static int eEnderOreMinY;
    public static int eEnderOreMaxY;
    public static int eEnderOreFrequency;
    public static int eEnderOreMaxSize;
    public static int eEnderOreMinSize;
    public static double eEndermanSpawn;
    public static boolean endOreGen;

    //Misc config
    public static boolean divineHammer;
    public static int smiteFail;
    public static int smiteRange;
    public static boolean fakePlayersSpawnEndermen;
    //TODO Add in these features
    //public static boolean versionCheckEnabled;
    //public static boolean retrogen;
    //public static boolean verboseSpawn;

    public static void setInstance(Configuration config) {
        ModConfig.config = config;
        readConfig(true);
    }

    public static Configuration getInstance() {
        return config;
    }

    @SubscribeEvent
    public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (EnderDustAndMore.MODID.equals(event.getModID()))
            readConfig(false);
    }

    public static void readConfig(boolean load) {
        try {
            if (load)
                config.load();
            readOverworldConfig();
            readNetherConfig();
            readEndConfig();
            readMiscConfig();
        } catch (Exception e1) {
            EnderDustAndMore.logger.error("I could not load the configuration file... I iz sad...", e1);
        } finally {
            if (config.hasChanged())
                config.save();
        }
    }

    private static void readOverworldConfig() {
        initCategory(CATEGORY_OVERWORLD, "Overworld generation configuration");
        overworldOreGen = getBoolean("overworldOreGen", CATEGORY_OVERWORLD, true, "Set to false to disable Ender Ore in the Overworld", LANG_KEY_ENDER_ORE_GEN);
        enderOreFrequency = getInt("enderOreFrequency", CATEGORY_OVERWORLD, 10, 0, 40, "Spawn frequency in chunks", LANG_KEY_ENDER_ORE_FREQUENCY);
        enderOreMinY = getInt("enderOreMinY", CATEGORY_OVERWORLD, 1, 1, 255, "Sets the minimum height the block will spawn at", LANG_KEY_ENDER_ORE_MIN_Y);
        enderOreMaxY = getInt("enderOreMaxY", CATEGORY_OVERWORLD, 10, 1, 255, "Sets the maximum height the block will spawn at", LANG_KEY_ENDER_ORE_MAX_Y);
        enderOreMaxSize = getInt("enderOreMaxSize", CATEGORY_OVERWORLD, 4, 0, 50, "Sets the maximum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MAX_SIZE);
        enderOreMinSize = getInt("enderOreMinSize", CATEGORY_OVERWORLD, 6, 0, 50,"Sets the minimum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MIN_SIZE);
        endermanSpawn = getDouble("endermanSpawn", CATEGORY_OVERWORLD, 0.25, 0.0, 1.0, "Enderman spawn chance in decimals", LANG_KEY_ENDERMAN_SPAWN);
    }

    private static void readNetherConfig() {
        initCategory(CATEGORY_NETHER, "Nether generation configuration");
        netherOreGen = getBoolean("netherOreGen", CATEGORY_NETHER, false, "Set to true to enable Ender Ore in the Nether", LANG_KEY_ENDER_ORE_GEN);
        nEnderOreFrequency = getInt("nEnderOreFrequency", CATEGORY_NETHER, 15, 0, 40, "Spawn frequency in chunks", LANG_KEY_ENDER_ORE_FREQUENCY);
        nEnderOreMinY = getInt("nEnderOreMinY", CATEGORY_NETHER, 60, 1, 255, "Sets the minimum height the block will spawn at", LANG_KEY_ENDER_ORE_MIN_Y);
        nEnderOreMaxY = getInt("nEnderOreMaxY", CATEGORY_NETHER, 90, 1, 255, "Sets the maximum height the block will spawn at", LANG_KEY_ENDER_ORE_MAX_Y);
        nEnderOreMaxSize = getInt("nEnderOreMaxSize", CATEGORY_NETHER, 6, 0, 50, "Sets the maximum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MAX_SIZE);
        nEnderOreMinSize = getInt("nEnderOreMinSize", CATEGORY_NETHER, 8, 0, 50, "Sets the minimum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MIN_SIZE);
        nEndermanSpawn = getDouble("nEndermanSpawn", CATEGORY_NETHER, 0.5, 0.0, 1.0, "Enderman spawn chance in decimals", LANG_KEY_ENDERMAN_SPAWN);
    }

    private static void readEndConfig() {
        initCategory(CATEGORY_END, "The End generation configuration");
        endOreGen = getBoolean("endOreGen", CATEGORY_END, false, "Set to true to enable Ender Ore in The End", LANG_KEY_ENDER_ORE_GEN);
        eEnderOreFrequency = getInt("eEnderOreFrequency", CATEGORY_END, 20, 0, 40, "Spawn frequency in chunks", LANG_KEY_ENDER_ORE_FREQUENCY);
        eEnderOreMinY = getInt("eEnderOreMinY", CATEGORY_END, 1, 1, 255, "Sets the minimum height the block will spawn at", LANG_KEY_ENDER_ORE_MIN_Y);
        eEnderOreMaxY = getInt("eEnderOreMaxY", CATEGORY_END, 60, 1, 255, "Sets the maximum height the block will spawn at", LANG_KEY_ENDER_ORE_MAX_Y);
        eEnderOreMaxSize = getInt("eEnderOreMaxSize", CATEGORY_END, 8, 0, 50, "Sets the maximum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MAX_SIZE);
        eEnderOreMinSize = getInt("eEnderOreMinSize", CATEGORY_END, 10, 0, 50, "Sets the minimum amount of blocks that will spawn in one cluster", LANG_KEY_ENDER_ORE_MIN_SIZE);
        eEndermanSpawn = getDouble("eEndermanSpawn", CATEGORY_END, 0.75, 0.0, 1.0, "Enderman spawn chance in decimals", LANG_KEY_ENDERMAN_SPAWN);
    }

    private static void readMiscConfig() {
        initCategory(CATEGORY_MISC, "Miscellaneous configuration");
        divineHammer = getBoolean("divineHammer", CATEGORY_MISC, true, "Set to false to disable the Divine Hammer");
        setRequiresMcRestart("divineHammer", CATEGORY_MISC);
        smiteFail = getInt("smiteFail", CATEGORY_MISC, 6, 2, 100, "Sets the failure chance when right clicking the Divine Hammer");
        smiteRange = getInt("smiteRange", CATEGORY_MISC, 130, 1, 320, "Sets the maximum range the Divine Hammer can smite");
        fakePlayersSpawnEndermen = getBoolean("fakePlayersSpawnEndermen", CATEGORY_MISC, true, "Set to false to prevent fake players (AKA machines) from spawning endermen");
        //TODO Add in these features
        //versionCheckEnabled = getBoolean("versionCheckEnabled", CATEGORY_MISC, true, "Should you not want to check for updates set this to false");
        //retrogen = getBoolean("retrogen", CATEGORY_MISC, true, "Enable this if you want to get retrogen (generation of ores) for already existing chunks");
        //verboseSpawn = getBoolean("verboseSpawn", CATEGORY_MISC, false, "Enable this if you want to see (in the log) where Ender Ore is spawned");
    }

    private static void initCategory(String name, String comment) {
        initCategory(name, comment, name);
    }

    private static void initCategory(String name, String comment, String langKey) {
        ConfigCategory category = config.getCategory(name);
        category.setComment(comment);
        category.setLanguageKey(LANG_PREFIX_CATEGORY + langKey);
    }

    private static boolean getBoolean(String name, String category, boolean defaultValue, String comment) {
        return getBoolean(name, category, defaultValue, comment, name);
    }

    private static boolean getBoolean(String name, String category, boolean defaultValue, String comment, String langKey) {
        return config.getBoolean(name, category, defaultValue, comment, LANG_PREFIX_PROPERTY + langKey);
    }

    private static int getInt(String name, String category, int defaultValue, int minValue, int maxValue, String comment) {
        return getInt(name, category, defaultValue, minValue, maxValue, comment, name);
    }

    private static int getInt(String name, String category, int defaultValue, int minValue, int maxValue, String comment, String langKey) {
        return config.getInt(name, category, defaultValue, minValue, maxValue, comment, LANG_PREFIX_PROPERTY + langKey);
    }

    private static double getDouble(String name, String category, double defaultValue, double minValue, double maxValue, String comment) {
        return getDouble(name, category, defaultValue, minValue, maxValue, comment, name);
    }

    private static double getDouble(String name, String category, double defaultValue, double minValue, double maxValue, String comment, String langKey) {
        Property prop = config.get(category, name, defaultValue);
        prop.setLanguageKey(LANG_PREFIX_PROPERTY + langKey);
        prop.setComment(comment + " [range: " + minValue + " ~ " + maxValue + ", default: " + defaultValue + "]");
        prop.setMinValue(minValue);
        prop.setMaxValue(maxValue);
        return MathHelper.clamp(prop.getDouble(defaultValue), minValue, maxValue);
    }

    private static Property getProperty(String name, String category) {
        return config.getCategory(category).get(name);
    }

    /**
     * Prevents this property from being edited while a world is running. The
     * property will only be editable from the config screen in the mods list
     * on the main menu.
     * <p>
     * Care should be taken to ensure that only properties that are truly
     * dynamic can be changed from the in-game options menu.
     */
    private static void setRequiresWorldRestart(String name, String category) {
        Property prop = getProperty(name, category);
        prop.setRequiresWorldRestart(true);
    }

    /**
     * Requires Minecraft to be restarted when this property is changed. This
     * will also disable editing of the property while a world is running.
     */
    private static void setRequiresMcRestart(String name, String category) {
        Property prop = getProperty(name, category);
        prop.setRequiresMcRestart(true);
    }
}