/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.util;

import io.github.proxysprojects.enderdustandmore.blocks.ModBlocks;
import io.github.proxysprojects.enderdustandmore.items.ModItems;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class ModRecipes {

    public static void init() {
        addRecipes();
        addSmelting();
    }

    private static void addRecipes() {
        GameRegistry.addRecipe(new ItemStack(Items.ENDER_PEARL, 1), " d ", "ddd", " d ", 'd', ModItems.ENDER_DUST);
        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.DUST_ENDER_BOTTLE), ModItems.ENDER_DUST, Items.GLASS_BOTTLE);
        if (ModConfig.divineHammer) {
            //TODO Change the recipe.
            GameRegistry.addRecipe(new ItemStack(ModItems.DIVINE_ROD, 1), "  r", " g ", "g  ", 'g', Items.GOLDEN_APPLE, 'r', Blocks.REDSTONE_BLOCK);
            GameRegistry.addRecipe(new ItemStack(ModItems.DIVINE_HAMMER_HEAD, 1), "gng", "rgr", "r", 'g', Items.GOLDEN_APPLE, 'r', Blocks.REDSTONE_BLOCK, 'n', Items.NETHER_STAR);
            GameRegistry.addRecipe(new ItemStack(ModItems.DIVINE_HAMMER, 1), " rh", "rnr", "dr ", 'h', ModItems.DIVINE_HAMMER_HEAD, 'd', ModItems.DIVINE_ROD, 'r', Items.REDSTONE);
        }
    }

    private static void addSmelting() {
        GameRegistry.addSmelting(ModBlocks.BLOCK_ENDER_ORE, new ItemStack(ModItems.ENDER_DUST), 1.0F);
        GameRegistry.addSmelting(ModItems.DUST_ENDER_BOTTLE, new ItemStack(ModItems.LIQUID_ENDER_BOTTLE), 0.0F);
    }
}
