/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.util;

import io.github.proxysprojects.enderdustandmore.blocks.BlockBaseFluid;
import io.github.proxysprojects.enderdustandmore.client.StateMapper;
import io.github.proxysprojects.enderdustandmore.fluids.FluidBase;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public final class ModFluids {
    private static final List<Fluid> FLUIDS = new ArrayList<>();

    public static void addFluid(Fluid fluid) {
        FLUIDS.add(fluid);
    }

    public static List<Fluid> getFluids() {
        return FLUIDS;
    }

    public static final FluidBase ENDER = new FluidBase("Ender", 2000, 2500, 1000, 10, 0xFF0A523B);

    public static void init() {
        for (Fluid fluid : getFluids()) {
            FluidRegistry.registerFluid(fluid);
            FluidRegistry.addBucketForFluid(fluid);
            Block block = new BlockBaseFluid("Molten" + StringUtils.capitalize(fluid.getName()), fluid);
            fluid.setBlock(block);
            GameRegistry.register(block);
            GameRegistry.register(new ItemBlock(block), block.getRegistryName());
        }
    }

    @SideOnly(Side.CLIENT)
    public static void setModels() {
        for (Fluid fluid : getFluids()) {
            Block block = fluid.getBlock();
            Item item = Item.getItemFromBlock(block);
            if (item == null)
                throw new NullPointerException("Could not get item from block in ModFluids#bakeModels()");
            ModelResourceLocation fluidModelLocation = new ModelResourceLocation(block.getRegistryName(), "fluid");
            ModelLoader.setCustomStateMapper(block, new StateMapper((s) -> fluidModelLocation));
            ModelLoader.registerItemVariants(item);
            ModelLoader.setCustomMeshDefinition(item, (s) -> fluidModelLocation);
        }
    }
}