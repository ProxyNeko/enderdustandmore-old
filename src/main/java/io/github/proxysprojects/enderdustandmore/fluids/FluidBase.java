/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.fluids;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import io.github.proxysprojects.enderdustandmore.util.ModFluids;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraftforge.fluids.Fluid;
import net.minecraftforge.fluids.FluidStack;

public class FluidBase extends Fluid {

    private static final ResourceLocation STILL_TEXTURE_LOCATION = new ResourceLocation(EnderDustAndMore.MODID, "blocks/fluid_still");
    private static final ResourceLocation FLOWING_TEXTURE_LOCATION = new ResourceLocation(EnderDustAndMore.MODID, "blocks/fluid_still");
    private int color = 0xFFFFFFFF;

    public FluidBase(String name, int density, int viscosity, int temperature, int luminosity, int color) {
        this(name, STILL_TEXTURE_LOCATION, FLOWING_TEXTURE_LOCATION);
        setDensity(density);
        setViscosity(viscosity);
        setTemperature(temperature);
        setLuminosity(luminosity);
        setUnlocalizedName(name);
        setColor(color);
    }

    public FluidBase(String name, ResourceLocation still, ResourceLocation flowing) {
        super(name, still, flowing);
        ModFluids.addFluid(this);
    }

    public FluidBase setColor(int color) {
        this.color = color;
        return this;
    }

    @Override
    public int getColor() {
        return color;
    }

    @Override
    public String getLocalizedName(FluidStack stack) {
        String s = getUnlocalizedName(stack);
        return s == null ? "" : I18n.translateToLocal(s + ".name");
    }
}
