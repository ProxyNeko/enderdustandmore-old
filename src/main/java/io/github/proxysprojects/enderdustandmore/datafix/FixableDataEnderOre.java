/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.datafix;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import io.github.proxysprojects.enderdustandmore.blocks.BlockEnderOre;
import io.github.proxysprojects.enderdustandmore.blocks.ModBlocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.datafix.IFixableData;
import net.minecraftforge.common.util.Constants;

public class FixableDataEnderOre implements IFixableData {
    @Override
    public int getFixVersion() {
        return 1;
    }

    @Override
    public NBTTagCompound fixTagCompound(NBTTagCompound compound) {
        if (compound.hasKey("id", Constants.NBT.TAG_STRING)) {
            String id = compound.getString("id");
            ResourceLocation registryName = new ResourceLocation(id);
            String modId = registryName.getResourceDomain();
            String name = registryName.getResourcePath();
            if (modId.equals(EnderDustAndMore.MODID) && (name.equals("nether_ender_ore") || name.equals("end_ender_ore"))) {
                String newId = ModBlocks.BLOCK_ENDER_ORE.getRegistryName().toString();
                int newMeta;
                switch (name) {
                    default:
                        newMeta = BlockEnderOre.Variant.NORMAL.getMeta();
                        break;
                    case "nether_ender_ore":
                        newMeta = BlockEnderOre.Variant.NETHER.getMeta();
                        break;
                    case "end_ender_ore":
                        newMeta = BlockEnderOre.Variant.END.getMeta();
                        break;
                }
                compound.setString("id", newId);
                compound.setShort("Damage", (short) newMeta);
            }
        }
        return compound;
    }
}
