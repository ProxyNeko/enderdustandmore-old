/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.datafix;

import io.github.proxysprojects.enderdustandmore.EnderDustAndMore;
import net.minecraft.util.datafix.FixTypes;
import net.minecraftforge.common.util.CompoundDataFixer;
import net.minecraftforge.common.util.ModFixs;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class ModDataFixesManager {
    /**
     * The current data version for this mod. Must be incremented with
     * each change and/or addition to this mod's data fixers.
     * <p>
     * The fix version for each data fixer must correspond to the
     * version in which it was added or changed. For example,
     * <tt>EnderOreFixableData</tt> was added in version 1, so its
     * original fix version was 1. If any changes are made to
     * <tt>EnderOreFixableData</tt> in version 123, its new fix version
     * will be 123.
     */
    public static final int MOD_DATA_VERSION = 1;

    public static void init() {
        CompoundDataFixer dataFixer = FMLCommonHandler.instance().getDataFixer();
        ModFixs modDataFixer = dataFixer.init(EnderDustAndMore.MODID, MOD_DATA_VERSION);
        modDataFixer.registerFix(FixTypes.ITEM_INSTANCE, new FixableDataEnderOre());
    }
}
