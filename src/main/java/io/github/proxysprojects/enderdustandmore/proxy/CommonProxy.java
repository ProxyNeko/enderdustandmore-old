/*
 * The MIT License (MIT)
 * Copyright (c) 2017 ProxyNeko
 * https://proxysprojects.github.io
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
 * IN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package io.github.proxysprojects.enderdustandmore.proxy;

import io.github.proxysprojects.enderdustandmore.blocks.ModBlocks;
import io.github.proxysprojects.enderdustandmore.datafix.ModDataFixesManager;
import io.github.proxysprojects.enderdustandmore.util.ModConfig;
import io.github.proxysprojects.enderdustandmore.util.ModFluids;
import io.github.proxysprojects.enderdustandmore.util.ModRecipes;
import io.github.proxysprojects.enderdustandmore.util.ModWorldGenManager;
import io.github.proxysprojects.enderdustandmore.items.ModItems;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class CommonProxy {

    public void preInit(FMLPreInitializationEvent event) {
        ModConfig.setInstance(new Configuration(event.getSuggestedConfigurationFile()));
        ModBlocks.init();
        ModItems.init();
        ModFluids.init();
        ModRecipes.init();
    }

    public void init(FMLInitializationEvent event) {
        ModWorldGenManager.init();
        ModDataFixesManager.init();
    }

    public void postInit(FMLPostInitializationEvent event) {
        if (ModConfig.getInstance().hasChanged())
            ModConfig.getInstance().save();
    }

    public boolean isPaused() {
        return false;
    }
}