[![](http://cf.way2muchnoise.eu/full_ender-dust-more_downloads.svg)](http://minecraft.curseforge.com/projects/ender-dust-more) [![](http://cf.way2muchnoise.eu/versions/Minecraft_ender-dust-more_all.svg)](http://minecraft.curseforge.com/projects/ender-dust-more)

# [Ender Dust & More](https://minecraft.curseforge.com/projects/ender-dust-more)
This mod adds Ender Ore to Minecraft to give players the ability to craft Ender Pearls but beware of the creatures that lurk in stone as they may not be happy to be kicked out of their home...